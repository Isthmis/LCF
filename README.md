# LCF

LCF - serif typeface code font

![Preview](Preview.png) 


## License

Created by (c) 2018 Mutous

Derived from [Linux Libertine](http://www.linuxlibertine.org) (c) 2003 Philipp H. Poll  

This project is licensed under the terms of the SIL Open Font License v1.1 / GNU General Public License v2.0.

<a href="http://scripts.sil.org/OFL"><img src="http://scripts.sil.org/cms/sites/nrsi/media/OFL_logo_rect_color.png" alt="SIL OFL" /></a>

<a href="https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"><img src="https://upload.wikimedia.org/wikipedia/commons/4/4b/License_icon-gpl-88x31-2.svg" alt="GPLv2" width="88" height="31"></a>
